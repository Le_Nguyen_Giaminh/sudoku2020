(ns mrsudoku.solver
  (:use midje.sweet)
  (:require [mrsudoku.grid :as g])
  (:require [mrsudoku.engine :as eng]))

(def ^:private sudoku-grid (var-get #'g/sudoku-grid))

(defn find-empty
  "Trouve une case qui n'est pas encore été remplie dans la grille"
  [grid]
  (let [cases (for [cx (range 1 10)
                    cy (range 1 10)
                    :when (nil? (get (g/cell grid cx cy) :value))]
                [cx cy])]
    (first cases)))

(fact
 (find-empty sudoku-grid) => [1 3])

(defn find-block
  "Trouve la ref du bloc, cx est la colonne et cy est la ligne"
  [cx cy]
  (cond
    (<= 1 cx 3) (cond
                  (<= 1 cy 3) 1
                  (<= 4 cy 6) 4
                  :else 7)
    (<= 4 cx 6) (cond
                  (<= 1 cy 3) 2
                  (<= 4 cy 6) 5
                  :else 8)
    (<= 7 cx 9) (cond
                  (<= 1 cy 3) 3
                  (<= 4 cy 6) 6
                  :else 9)))

(defn available-number
  "Trouve les nombres qui peuvent être mis dans la case, cx est le numéro de la colonne et cy est le numéro de la ligne"
  [grid cx cy]
  (let [col (eng/values (g/col grid cx))
        row (eng/values (g/row grid cy))
        block-ref (find-block cx cy)
        bloc (eng/values (g/block grid block-ref))
        total (clojure.set/union col row bloc)]
    (clojure.set/difference #{1 2 3 4 5 6 7 8 9} total)))

(fact
 (available-number sudoku-grid 1 3) => #{1 2})

(defn solve
  "Solver du sudoku"
  ([grid] (let [pos (find-empty grid)]
            (if (empty? pos)
              grid
              (solve [] grid pos (available-number grid (first pos) (second pos))))))
  ([prev-grids grid pos a-tester] (if (seq a-tester)
                                    (let [new-cell {:status :set, :value (first a-tester)}
                                          new-grid (g/change-cell grid (first pos) (second pos) new-cell)
                                          new-pos (find-empty new-grid)]
                                      (if (empty? new-pos)
                                        new-grid
                                        (let [new-prev {:grid new-grid, :pos pos, :a-tester (rest a-tester)}]
                                          (solve (conj prev-grids new-prev) new-grid new-pos (available-number new-grid (first new-pos) (second new-pos))))))
                                    (if (empty? prev-grids)
                                      grid
                                      (let [nb (count prev-grids)
                                            prev (get prev-grids (- nb 1))]
                                        (solve (subvec prev-grids 0 (- nb 1)) (get prev :grid) (get prev :pos) (get prev :a-tester)))))))

(solve sudoku-grid)









